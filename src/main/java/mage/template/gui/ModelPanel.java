package mage.template.gui;

import mage.template.MyMage;
import mage.ui.gui.SystemOutEnvironmentPanel;

import javax.swing.*;
import java.awt.*;

public class ModelPanel extends mage.ui.gui.ModelPanel {

    private static final long serialVersionUID = 4085488855996804901L;

    public ModelPanel() {
        super();

        this.setName("Hello World");
    }

    @Override
    protected JPanel getOverallPanel() {
        JPanel overallPanel = new JPanel(new GridBagLayout());
        overallPanel.setName("Overall");

        MyMage defaultMage = (MyMage) getMage();
        JPanel environmentPanel = new SystemOutEnvironmentPanel();
        overallPanel.add(environmentPanel);
        return overallPanel;
    }
}
