package mage.template.gui;

import mage.ui.gui.MagePlatformFrame;

import javax.swing.*;
import java.io.IOException;

/**
 * 
 * @author Önder Gürcan
 *
 */
public class Application {
	public static void main(String[] args) throws IOException {

		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new MagePlatformFrame() {
			private static final long serialVersionUID = -5555992681503766712L;

			@Override
			protected mage.ui.gui.ModelPanel createModelPanel() {
				return new ModelPanel();
			}
		};

		// Display the window
		frame.setVisible(true);

		frame.pack();
	}
}
