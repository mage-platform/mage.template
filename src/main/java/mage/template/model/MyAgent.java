package mage.template.model;

import mage.api.Agent;

/**
 * The {@code MyAgent} class represents an agent that extends the base {@link Agent} class.
 * This agent adopts a {@link Greeter} behavior and executes a greeting action indefinitely.
 *
 * The {@code Greeter} class is assumed to handle interactions with the environment
 * and execute specific tasks like greeting in a loop.
 */
public class MyAgent extends Agent {

    /**
     * Default constructor for {@code MyAgent} which invokes the parent {@link Agent} constructor.
     */
    public MyAgent() {
        super();
    }

    /**
     * This method is called when the agent is being set up. It adopts a {@link Greeter} behavior
     * and executes a greeting action in a loop with a specified delay.
     */
    @Override
    protected void setup() {
        // Adopt the Greeter role for this agent, to be played within the HW_Environment.
        adopt(new Greeter(this, "HW_Environment"));

        // Access the Greeter behavior and schedules the greet() action indefinitely.
        // The action is repeated every 5 ticks starting from tick 4.
        as(Greeter.class).greet().executeForever(4, 5);

        // Access the Greeter behavior and schedules the greetByMessage() action.
        // The action is executed at tick 15.
        as(Greeter.class).greetByMessage().executeAt(15);
    }
}
