package mage.template.model;

import mage.api.Action;
import mage.api.Agent;
import mage.api.Message;
import mage.api.Role;

/**
 * The Greeter class is a specific role that an agent can adopt.
 * It enables the agent to perform greeting actions and handle incoming messages.
 * This class extends the Role class and provides two types of greeting actions:
 * a simple greeting and a message-based greeting.
 * It also includes functionality for handling incoming SIGNAL messages.
 */
public class Greeter extends Role {

    /**
     * Constructs a Greeter role for a specified agent and environment.
     *
     * @param owner The agent that owns this role.
     * @param environmentName The name of the environment in which this role is being used.
     */
    public Greeter(Agent owner, String environmentName) {
        super(owner, environmentName);
    }

    /**
     * Creates a new action where the agent will log a simple "Hello world!" message.
     *
     * @return An Action representing the greeting behavior.
     */
    final public Action greet() {
        return newAction(() -> {
            getLogger().info("says \"Hello world!\"");
        });
    }

    /**
     * Creates a new action where the agent sends a SIGNAL message with a greeting
     * to another agent identified as "MyAgent1".
     *
     * The method retrieves the identifier of the target agent's Greeter role and
     * sends a message with the payload "Hey !" to that agent.
     *
     * @return An Action representing the message-based greeting behavior.
     */
    final public Action greetByMessage() {
        return newAction(() -> {
            var myAgent = getAgent(MyAgent.class, "MyAgent-1");
            var receiverID = myAgent.as(Greeter.class).getIdentifier();
            sendMessage("SIGNAL", "Hey !", receiverID);
        });
    }

    /**
     * Handles incoming SIGNAL messages by logging the sender's agent name and the
     * content of the message payload.
     *
     * @param message The SIGNAL message received, containing the sender information and payload.
     */
    public void handleSIGNALMessage(Message<String> message) {
        getLogger().info("received message from {} saying {}.", message.getSender().getAgentName(), message.getPayload());
    }
}
