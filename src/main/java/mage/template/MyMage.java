package mage.template;

import mage.api.Environment;
import mage.api.Mage;
import mage.template.model.MyAgent;

/**
 * HelloWorldMage class is a simple implementation of a {@link Mage}.
 * It creates an environment and adds a greeting agent to it.
 * This class demonstrates how to set up an environment and populate it
 * with agents in the context of the MAGE framework.
 *
 * @author Önder Gürcan
 */
public class MyMage extends Mage {

	/**
	 * This method is called to set up the environment and agents.
	 * It creates an environment named "HW_Environment" and adds an instance
	 * of {@link MyAgent} to this environment.
	 */
	@Override
	protected void setup() {
		// First, create an environment named "HW_Environment"
		var hwEnvironment = create(new Environment("HW_Environment"));

		// Create an instance of MyAgent, an agent capable of greeting
		var agent = create(new MyAgent());

		// Add the agent to the "HW_Environment"
		hwEnvironment.add(agent);
	}
}
