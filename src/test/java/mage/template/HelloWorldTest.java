package mage.template;

import mage.api.Agent;
import mage.api.Environment;
import mage.api.Mage;
import mage.api.role.Bystander;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

import java.nio.file.Path;

import static mage.api.Params.clearParameters;
import static mage.ui.test.TestInterface.launch;

/**
 * To find errors, gaps, or missing requirements in comparison to the actual
 * requirements, we can develop tests for our simulation model elements.
 *
 * <p>
 * To do so:
 *
 * <ol>
 * <li>Create a test class
 * <li>Write a test method:
 * <ol>
 * <li>Create and setup a mage
 * <li>Call method
 * {@link mage.ui.test.TestInterface#launch(Mage, int, TestInfo)}
 * </ol>
 * <li>Execute the test using your IDE or command
 * 
 * <pre>
 * mvn clean test
 * </pre>
 * </ol>
 *
 * @author Önder Gürcan
 */
public class HelloWorldTest {

	/** Set some required parameters for all tests. */
	@BeforeAll
	public static void beforeAll(@TempDir final Path resultDir) {
		clearParameters();
	}

	/**
	 * This test will use the experimenter we defined.
	 *
	 * <p>
	 * The experiment duration is set to 100 ticks.
	 *
	 * @param testInfo Injected by JUnit5
	 * @throws Throwable Any error occurring during the simulation
	 */
	@Test
	public void testUsingAnExistingExperimenter(final TestInfo testInfo) throws Throwable {
		launch(new MyMage(), 100, testInfo);
	}

	/**
	 * This time we define an anonymous experimenter and setup its scenario.
	 *
	 * <p>
	 * Contrary to {@link MyMage} scenario, the environment will be
	 * named 'HW Environment-Test' and the agent 'Alice'. Otherwise, the experiment
	 * is the same.
	 *
	 * @param testInfo Injected by JUnit5
	 * @throws Throwable Any error occurring during the simulation
	 */
	@Test
	public void testUsingAnAnonymousExperimenter(final TestInfo testInfo) throws Throwable {
		final var experimenter = new Mage() {

			@Override
			protected void setup() {
				// First we create our environment
				final var environment = create(new Environment("HW Environment-Test"));

				// We can now create our agent
				var agent = create(new Agent("Alice") {
					
					private Bystander bystanderRole;
					
					@Override
					public void initialize() {
						super.initialize();
						
						bystanderRole = new Bystander(this, environment);
						adopt(bystanderRole);
					}

					/*
					 * The plan will contain two actions: one to join our custom environment and our
					 * ACHelloWorld action.
					 */
					@Override
					protected void setup() {
						bystanderRole.log("Hello World !").executeAt(5);
					}

				});

				environment.add(agent);
			}
		};
		launch(experimenter, 100, testInfo);
	}

	/**
	 * We can even mix both: create an anonymous experimenter extending an existing
	 * experimenter.
	 *
	 * <p>
	 * Here we are modifying the {@link MyMage} scenario by adding a
	 * second agent named 'Alice'. Alice will say hello at tick 10.
	 *
	 * @param testInfo Injected by JUnit5
	 * @throws Throwable Any error occurring during the simulation
	 */
	@Test
	public void testWithAnAnonymousExperimenterExtendingHWExperimenter(final TestInfo testInfo) throws Throwable {
		final var experimenter = new MyMage() {

			@Override
			protected void setup() {
				super.setup();

				// then Alice
				final var alice = create(new Agent("Alice") {
					
					private Bystander bystanderRole;
					
					@Override
					public void initialize() {
						super.initialize();
						
						bystanderRole = new Bystander(this, hwEnvironment);
						adopt(bystanderRole);
					}
					
					@Override
					protected void setup() {
						bystanderRole.log("Hello World !").executeAt(10);
					}
				});
			}
		};
		launch(experimenter, 100, testInfo);
	}
}
