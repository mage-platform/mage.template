# declare the variables
templatePath="https://gitlab.com/mage-platform/mage.template/-/raw/master"
projectName=`basename $PWD`

wget $templatePath/.classpath

# set up the .gitignore file
wget $templatePath/.gitignore
# uncomment the commented necessary lines
sed -i 's/#.classpath/.classpath/g' .gitignore
sed -i 's/#.project/.project/g' .gitignore
sed -i 's/#.settings/.settings/g' .gitignore


wget $templatePath/.gitlab-ci.yml

# set up the .m2/ folder
mkdir .m2
cd .m2
wget $templatePath/.m2/ci_settings.xml
cd ..

# set up the .project file
rm .project
wget $templatePath/.project
# after replace the project name in .project with your projects name
sed -i.bak "s/mage.template/$projectName/g" .project && rm .project.bak

# set up the .settings/ folder
mkdir .settings
cd .settings
wget $templatePath/.settings/org.eclipse.jdt.core.prefs
wget $templatePath/.settings/org.eclipse.m2e.core.prefs
cd ..

# set up the pom.xml file
wget $templatePath/pom.xml
# here try to get the project name from folder name
# modify the artifact name
sed -i.bak "s/mage.template/$projectName/g" pom.xml && rm pom.xml.bak

# set up the default.json file
wget $templatePath/config.json
# after replace the project name in default.json with your projects name
sed -i.bak "s/mage.template/$projectName/g" config.json && rm config.json.bak

# set up the README.md file
wget $templatePath/README.md

# set up the src/main/java folder
mkdir src
cd src
mkdir main
cd main
mkdir java

# set up the src/main/resources folder
mkdir resources
cd resources
wget $templatePath/src/main/resources/logback.xml
wget $templatePath/src/main/resources/mage.api-2024.08.20.jar
wget $templatePath/src/main/resources/mage.ui-2024.08.20.jar
mkdir config
cd config
wget $templatePath/src/main/resources/config/default.json
sed -i.bak "s/mage.template/$projectName/g" default.json && rm default.json.bak
cd ..
cd ..
cd ..
cd ..

# set up the src/test/java folder
cd src
mkdir test
cd test
mkdir java

# set up the src/test/resources folder
mkdir resources
cd resources
# wget $templatePath/src/test/resources/log4j.properties
cd ..
cd ..
cd ..

# download the script for reinstalling mage-api module to maven
wget $templatePath/.mage-api-install.sh
chmod +x .mage-api-install.sh

# the use it to install the .jar dependency to maven
./.mage-api-install.sh

# set up the project's package folder
cd src/main/java
packageFolder=${projectName//./\/} # here replace '.' and '/'
mkdir -p $packageFolder
cd $packageFolder
wget $templatePath/src/main/java/mage/template/HelloWorldMage.java
sed -i.bak "s/mage.template/$projectName/g" HelloWorldMage.java && rm HelloWorldMage.java.bak
mkdir gui
cd gui
wget $templatePath/src/main/java/mage/template/gui/Application.java
sed -i.bak "s/mage.template/$projectName/g" Application.java && rm Application.java.bak
wget $templatePath/src/main/java/mage/template/gui/ModelPanel.java
sed -i.bak "s/mage.template/$projectName/g" ModelPanel.java && rm ModelPanel.java.bak

cd ../../../../../
cd src/test/java
packageFolder=${projectName//./\/} # here replace '.' and '/'
mkdir -p $packageFolder
cd $packageFolder
wget $templatePath/src/test/java/mage/template/HelloWorldTest.java
sed -i.bak "s/mage.template/$projectName/g" HelloWorldTest.java && rm HelloWorldTest.java.bak

