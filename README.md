# My MAGE Project

This is my MAGE project.

## Prerequisites

`git`, `mvn`, `wget`.

## Usage

First download `wget` or make sure that `wget` is installed and is in the path.

Then, execute the following command to download the script:

```bash
wget https://gitlab.com/mage-platform/mage.template/-/raw/master/.new-mage-project.sh
```

Make the script executable:

```bash
chmod +x .new-mage-project.sh
```

After, execute the script to configure your MAGE project:

```bash
./.new-mage-project.sh
```

Note that it will take the parent folder's name as `artifactID`.

Now, go back to your IDE and refresh the project. 

Finally, after the project building is done, go to Terminal and remove the script:

```bash
rm .new-mage-project.sh
```

## How to run?

You can either run the default experimenter using the following command:

```bash
> mvn clean compile exec:java
```

or you can run it (or another one) explicitly using the following command:

```bash
> mvn clean compile exec:java -Dexec.mainClass="mage.ui.Main" -Dexec.args="exec-cli config/config.json"
```


