#!/bin/bash

echo "Verifying if the required tools are installed or not..."

# Function to check if a command exists
check_command() {
    command -v $1 >/dev/null 2>&1
}

# Function to check if Java version is 21 or higher
check_java_version() {
    if check_command java; then
        java_version=$(java -version 2>&1 | awk -F[\".] '/version/ {print $2}')
        if [ "$java_version" -ge 21 ]; then
            echo "Java version 21 or higher is installed."
        else
            echo "Java version is below 21. Please upgrade."
            return 1
        fi
    else
        echo "Java is not installed."
        return 1
    fi
    return 0
}

# List of tools to check
tools=("git" "mvn" "wget")

# Flag to track if any tool is missing
missing_tools=0

# Loop through the list of tools and check each one
for tool in "${tools[@]}"; do
    if check_command $tool; then
        echo "$tool is installed."
    else
        echo "$tool is not installed."
        missing_tools=1
    fi
done

# Check for Java 21 or higher
if ! check_java_version; then
    missing_tools=1
fi

# Exit if any tools are missing
if [ $missing_tools -eq 1 ]; then
    echo "Exiting script. One or more required tool(s) are not installed."
    exit 1
fi

echo "All required tools are installed."

# Exit on any error
set -e

# URL of the mage.template project
REPO_URL="https://gitlab.com/mage-platform/mage.template.git"

# Step 1: Get the project name from the user
read -p "Enter the desired MAGE Platform project name: " PROJECT_NAME

# Step 2: Clone the repository
echo "Cloning the repository from $REPO_URL..."
git clone "$REPO_URL"

# Get the name of the cloned folder (mage.template)
PROJECT_FOLDER="mage.template"

# Step 3: Check if the project was cloned correctly
if [ ! -d "$PROJECT_FOLDER" ]; then
  echo "Error: Failed to clone mage.template project."
  exit 1
else
  echo "Repository cloned successfully into $PROJECT_FOLDER."
fi

# Step 4: Rename the project folder to the user-provided project name
echo "Renaming project folder from $PROJECT_FOLDER to $PROJECT_NAME..."
mv "$PROJECT_FOLDER" "$PROJECT_NAME"
echo "Folder renamed to $PROJECT_NAME."

# Step 5: Navigate into the project folder
echo "Navigating into the project folder $PROJECT_NAME..."
cd "$PROJECT_NAME"

# Step 6: Remove the existing git configuration
echo "Removing the existing git configuration..."
rm -rf .git
echo "Git configuration removed."

# Step 7: Find and replace occurrences of 'mage.template' in all files
echo "Replacing all occurrences of 'mage.template' with '$PROJECT_NAME' in the project files..."
FILES=$(grep -rl 'mage.template' .)

if [ -n "$FILES" ]; then
  for FILE in $FILES; do
    echo "Processing file: $FILE"
    sed -i.bak "s/mage.template/$PROJECT_NAME/g" "$FILE" && rm "$FILE".bak
  done
else
  echo "No occurrences of 'mage.template' found."
fi

echo "All occurrences replaced."

# Step 8: Rename the package directories from mage/template to match the project name
echo "Renaming package directories 'mage/template' to '$PROJECT_NAME'..."
if [ -d "src/main/java/mage/template" ]; then
  mv src/main/java/mage/template "src/main/java/$PROJECT_NAME"
  rm -rf src/main/java/mage
  echo "Package directory 'mage/template' renamed to 'src/main/java/$PROJECT_NAME'."
else
  echo "No 'mage/template' directory found."
fi
if [ -d "src/test/java/mage/template" ]; then
  mv src/test/java/mage/template "src/test/java/$PROJECT_NAME"
  rm -rf src/test/java/mage
  echo "Package directory 'mage/template' renamed to 'src/test/java/$PROJECT_NAME'."
else
  echo "No 'mage/template' directory found."
fi

# Step 9: Installing the required mage jar files.
echo "Installing the required mage jar files."
mvn install:install-file -Dfile=src/main/resources/mage.api-2024.08.20.jar -DgroupId=mage-platform -DartifactId=mage.api -Dversion=2024.08.20 -Dpackaging=jar
mvn install:install-file -Dfile=src/main/resources/mage.ui-2024.08.20.jar -DgroupId=mage-platform -DartifactId=mage.ui -Dversion=2024.08.20 -Dpackaging=jar
echo "The required mage jar files are installed."

# Step 10: Completion message
echo "---------------------------------------------------------"
echo "New MAGE Platform project named '$PROJECT_NAME' is ready."
echo "---------------------------------------------------------"

# Done

